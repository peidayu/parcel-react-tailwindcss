import React from "react";
import { createRoot } from "react-dom/client";
import "./style.css";

function App() {
  return (
    <div className="h-full bg-gradient-to-r from-slate-500 to-slate-800 text-white flex items-center justify-center">
      <h2 className="text-4xl font-serif">React + tailwindcss</h2>
    </div>
  );
}

const root = createRoot(document.querySelector("#app"));
root.render(<App />);
